package storage

import (
	"bootcamp/article/models"
	"errors"
	"strings"
)

var ErrorAlreadyExist = errors.New("already exists")
var ErrorNotExist = errors.New("not exist")

type ArticleStorage map[int]models.Article

func (storage ArticleStorage) Add(entity models.Article) error {
	if _, ok := storage[entity.ID]; ok {
		return ErrorAlreadyExist
	} else {
		storage[entity.ID] = entity
		return nil
	}

}

func (storage ArticleStorage) GetByID(ID int) (models.Article, error) {
	var resp models.Article
	if article, ok := storage[ID]; ok {
		resp = article
		return resp, nil
	} else {
		return resp, ErrorNotExist
	}
}

func (storage ArticleStorage) GetAll() []models.Article {
	var resp []models.Article
	for _, value := range storage {
		resp = append(resp, value)
	}
	return resp
}

func (storage ArticleStorage) Search(str string) []models.Article {
	var resp []models.Article
	for _, v := range storage {
		if strings.Contains(v.Title, str) {
			resp = append(resp, v)
		}
	}
	return resp
}

func (storage ArticleStorage) Update(entity models.Article) error {
	if _, ok := storage[entity.ID]; ok {
		storage[entity.ID] = entity
		return nil
	} else {
		return ErrorNotExist
	}

}

func (storage ArticleStorage) Delete(ID int) error {
	if _, ok := storage[ID]; ok {
		delete(storage, ID)
	} else {
		return ErrorNotExist
	}
	return nil
}
