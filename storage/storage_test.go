package storage_test

import (
	"bootcamp/article/models"
	"bootcamp/article/storage"
	"testing"
	"time"
)

var Storage storage.ArticleStorage = make(storage.ArticleStorage)

func count() int {
	return len(Storage)
}
func TestStorage(t *testing.T) {
	c := count()
	ti := time.Now()
	article := models.Article{
		ID:        len(Storage),
		Content:   models.Content{Title: "Title", Body: "Body"},
		Author:    models.Person{Firstname: "John", Lastname: "Doe"},
		CreatedAt: &ti,
	}
	//Add
	err := Storage.Add(article)
	if err != nil {
		t.Errorf("Add FAILED: %v", err)
	}
	if i := count(); i != c+1 {
		t.Errorf("Add FAILED: expected count %d but got %d", c+1, count())
	}

	err = Storage.Add(article)
	if err != storage.ErrorAlreadyExist {
		t.Errorf("Add FAILED: expected %s but got %v", storage.ErrorAlreadyExist, err)
	}
	t.Log("Add Passed")

	//GetById

	//Positive
	id := article.ID
	article, err = Storage.GetByID(article.ID)
	if err != nil {
		t.Errorf("GetById FAILED: %v", err)
	}
	if article.ID != id {
		t.Errorf("GetById FAILED: expected article with id %d but got %d", 1, article.ID)
	}

	//Negative
	article, err = Storage.GetByID(99)
	if err == nil {
		t.Errorf("GetById FAILED: expected error %v but got %v", storage.ErrorNotExist, err)
	}
	if article.ID != 0 {
		t.Errorf("GetById FAILED: expected %v but got %v", models.Article{}, article)
	}

	t.Log("GetById Passed")

	//GetAll
	slice := Storage.GetAll()
	if len(slice) != count() {
		t.Errorf("GetAll FAILED: expected %v but got %v", count(), len(slice))
	}
	Storage = make(storage.ArticleStorage)
	slice = Storage.GetAll()
	if len(slice) != 0 {
		t.Errorf("GetAll FAILED: expected %v but got %v", 0, len(slice))
	}
	t.Log("GetAll Passed")

	//Search

	//Positive
	article = models.Article{
		ID:        len(Storage),
		Content:   models.Content{Title: "Title", Body: "Body"},
		Author:    models.Person{Firstname: "John", Lastname: "Doe"},
		CreatedAt: &ti,
	}
	Storage.Add(article)

	str := "le"
	slice = Storage.Search(str)

	if len(slice) != 1 {
		t.Errorf("Search FAILED: expected %v but got %v", len(Storage), len(slice))
	}

	//Negative
	slice = Storage.Search("1a")
	if len(slice) != 0 {
		t.Errorf("Search FAILED: expected %v but got %v", 0, len(slice))
	}

	t.Log("Search Passed")

	//Update

	//Positive
	err = Storage.Update(article)
	if err != nil {
		t.Errorf("Update FAILED: %v", err)
	}

	str = "updated body"
	article.Body = str
	Storage.Update(article)
	if Storage[article.ID].Body != str {
		t.Errorf("Update FAILED: expected %v but got %v", str, Storage[article.ID].Body)
	}

	//Negative
	article.ID = 100
	err = Storage.Update(article)
	if err == nil {
		t.Errorf("Update FAILED: expected error %v but got %v", storage.ErrorNotExist, err)
	}
	t.Log("Update Passed")

	//Delete

	//Positive
	err = Storage.Delete(id)
	if err != nil {
		t.Errorf("Add FAILED: %v", err)
	}

	if _, ok := Storage[id]; ok {
		t.Errorf("Delete FAILED: expected %v but got %v", nil, Storage[id])
	}

	//Negative
	err = Storage.Delete(100)
	if err == nil {
		t.Errorf("Delete FAILED: expected error %v but got %v", storage.ErrorNotExist, err)
	}
	t.Log("Delete Passed")

}
