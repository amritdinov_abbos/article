package main

import (
	"bootcamp/article/models"
	"bootcamp/article/storage"
	"fmt"
	"strconv"
	"time"
)

var inMemory storage.ArticleStorage
var mode string

func main() {
	inMemory = make(storage.ArticleStorage)
	menu()
}
func menu() {
	defer menu()

	fmt.Printf("Number of articles in storage: %v\n", len(inMemory))
	fmt.Println("1 - Add article")
	fmt.Println("2 - Update article")
	fmt.Println("3 - Delete article")
	fmt.Println("4 - Search article by Title")
	fmt.Println("5 - Get article by Id")
	fmt.Println("6 - Get all articles")

	fmt.Scan(&mode)

	switch mode {
	case "1":
		Add()
	case "2":
		Update()
	case "3":
		Delete()
	case "4":
		Search()
	case "5":
		GetById()
	case "6":
		GetAll()
	default:
		fmt.Printf("\n\nPlease choose correct command!\n\n")
	}
}
func Add() {
	var article models.Article
	fmt.Println("Enter Title")
	fmt.Scan(&article.Title)

	fmt.Println("Enter Body")
	fmt.Scan(&article.Body)

	fmt.Println("Enter authors firstname")
	fmt.Scan(&article.Author.Firstname)

	fmt.Println("Enter authors lastname")
	fmt.Scan(&article.Author.Lastname)

	t := time.Now()
	article.CreatedAt = &t

	article.ID = len(inMemory)
	err := inMemory.Add(article)
	if err != nil {
		fmt.Println(err)
	}
}
func Update() {
	var article models.Article
	fmt.Println("Enter Id")
	fmt.Scan(&article.ID)

	fmt.Println("Enter Title")
	fmt.Scan(&article.Title)

	fmt.Println("Enter Body")
	fmt.Scan(&article.Body)

	fmt.Println("Enter authors firstname")
	fmt.Scan(&article.Author.Firstname)

	fmt.Println("Enter authors lastname")
	fmt.Scan(&article.Author.Lastname)

	t := time.Now()
	article.CreatedAt = &t

	article.ID = len(inMemory)
	err := inMemory.Update(article)
	if err != nil {
		fmt.Println(err)
	}
}
func Delete() {
	var id string
	fmt.Println("Enter article Id:")
	fmt.Scan(&id)
	i, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		fmt.Println("Write correct id")
	} else {
		err = inMemory.Delete(int(i))
		if err != nil {
			fmt.Println(err)
		}
	}
}
func Search() {
	var str string
	fmt.Println("Search:")
	fmt.Scan(&str)
	articles := inMemory.Search(str)
	printArticlesSlice(articles)
}
func GetById() {
	var id string
	fmt.Println("Enter Id:")
	fmt.Scan(&id)
	i, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		fmt.Println("Please, enter correct id")
	} else {
		article, err := inMemory.GetByID(int(i))
		if err != nil {
			fmt.Println(err)
		}
		printArticlesSlice([]models.Article{article})
	}
}
func GetAll() {
	articles := inMemory.GetAll()
	printArticlesSlice(articles)
}
func printArticlesSlice(articles []models.Article) {
	for _, v := range articles {
		fmt.Printf("Article Id: %v \n", v.ID)
		fmt.Printf("Title: %v \n", v.Title)
		fmt.Printf("Body: %v \n", v.Body)
		fmt.Printf("Author Firtname: %v \n", v.Author.Firstname)
		fmt.Printf("Author Lastname: %v \n", v.Author.Lastname)
		fmt.Printf("UpdatedAt: %v \n\n\n", v.CreatedAt)

	}
}
