package main

import (
	"bootcamp/article/models"
	"bootcamp/article/storage"
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
)

var inMemory storage.ArticleStorage

func main() {

	gin.SetMode(gin.DebugMode)
	r := gin.New()
	r.Use(gin.Logger(), gin.Recovery())

	inMemory = make(storage.ArticleStorage)

	r.POST("/articles", CreateHandler)
	r.GET("/articles", GetAllHandler)
	r.GET("/articles/:id", GetByIDHandler)
	r.PUT("/articles/:id", UpdateHandler)
	r.DELETE("/articles/:id", DeleteHandler)
	r.GET("/articles/search/:query", SearchHandler)

	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
func SearchHandler(c *gin.Context) {
	txt := c.Param("query")
	fmt.Println(txt)
	resp := inMemory.Search(txt)

	c.JSON(200, gin.H{
		"res": resp,
	})
}
func CreateHandler(c *gin.Context) {
	var a1 models.Article
	err := c.BindJSON(&a1)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
	}
	err = inMemory.Add(a1)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.String(200, "Ok")
}
func GetAllHandler(c *gin.Context) {
	resp := inMemory.GetAll()
	c.JSON(200, gin.H{
		"res": resp,
	})
	return
}
func GetByIDHandler(c *gin.Context) {
	idStr := c.Param("id")
	fmt.Println(idStr)

	idNum, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	resp, err := inMemory.GetByID(int(idNum))
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(200, gin.H{
		"res": resp,
	})
}
func UpdateHandler(c *gin.Context) {
	idStr := c.Param("id")
	idNum, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	var a1 models.Article
	err = c.BindJSON(&a1)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
	}
	if a1.ID == int(idNum) {
		err = inMemory.Update(a1)
		if err != nil {
			c.JSON(400, gin.H{
				"error": err.Error(),
			})
		}
	} else {
		c.JSON(400, gin.H{
			"error": "id param and article id doesn't match!",
		})
		return
	}

	article, err := inMemory.GetByID(a1.ID)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
	}
	c.JSON(200, gin.H{
		"res": article,
	})
}
func DeleteHandler(c *gin.Context) {
	idStr := c.Param("id")
	idNum, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	fmt.Println(idNum)
	err = inMemory.Delete(int(idNum))
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(200, "ok")
}
